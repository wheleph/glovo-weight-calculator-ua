package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Console;
import java.io.PrintStream;
import java.util.Scanner;

public class DefaultLoggingConsole implements LoggingConsole {
    private final PrintStream out = System.out;
    private final PrintStream err = System.err;
    private final Scanner scanner = new Scanner(System.in);
    private final Console console = System.console();

    Logger logger = LogManager.getLogger(DefaultLoggingConsole.class);

    public void print(String message) {
        out.print(message);
        logger.info(message);
    }

    @Override
    public void println(String message) {
        out.println(message);
        logger.info(message);
    }

    @Override
    public void printf(String format, Object... args) {
        String formattedString = String.format(format, args);
        out.print(formattedString);
        logger.info(formattedString);
    }

    @Override
    public void errPrint(String message) {
        err.print(message);
        logger.error(message);
    }

    @Override
    public void errPrintln(String message) {
        err.println(message);
        logger.error(message);
    }

    @Override
    public void errPrintf(String format, Object... args) {
        String formattedString = String.format(format, args);
        err.print(formattedString);
        logger.error(formattedString);
    }

    public String readLine() {
        String line = scanner.nextLine();
        logger.info(line);
        return line;
    }

    @Override
    public String readPassword(String prompt) {
        String passwordLine;
        String displayedPrompt;

        if (console != null) {
            displayedPrompt = prompt;
            passwordLine = new String(console.readPassword(prompt));
        } else {
            displayedPrompt = String.format("[Echo on] %s", prompt);
            out.print(displayedPrompt);
            passwordLine = scanner.nextLine();
        }
        logger.info(displayedPrompt + "****");
        return passwordLine;
    }
}
