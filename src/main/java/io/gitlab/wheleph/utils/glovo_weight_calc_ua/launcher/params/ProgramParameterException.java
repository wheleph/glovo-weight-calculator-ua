package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params;

public class ProgramParameterException extends Exception {

    public ProgramParameterException(String message) {
        super(message);
    }

    public ProgramParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
