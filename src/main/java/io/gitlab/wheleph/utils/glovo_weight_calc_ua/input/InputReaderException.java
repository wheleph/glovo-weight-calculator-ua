package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input;

public class InputReaderException extends Exception {
    public InputReaderException(String message) {
        super(message);
    }

    public InputReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
