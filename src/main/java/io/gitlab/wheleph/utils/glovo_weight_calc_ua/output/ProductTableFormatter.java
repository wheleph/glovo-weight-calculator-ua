package io.gitlab.wheleph.utils.glovo_weight_calc_ua.output;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;

import java.util.List;

public interface ProductTableFormatter {
    String format(List<Product> productList);
}
