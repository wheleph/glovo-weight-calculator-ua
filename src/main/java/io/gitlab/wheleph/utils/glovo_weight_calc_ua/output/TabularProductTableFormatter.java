package io.gitlab.wheleph.utils.glovo_weight_calc_ua.output;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;

import java.text.DecimalFormat;
import java.util.List;

public class TabularProductTableFormatter implements ProductTableFormatter {
    private static final DecimalFormat WEIGHT_FORMAT = new DecimalFormat("#.##");

    @Override
    public String format(List<Product> productList) {
        StringBuilder outputBuilder = new StringBuilder();

        if (productList.isEmpty()) {
            outputBuilder.append("No products available\n");
        } else {
            int totalWeightGrams = 0;
            boolean hasUnknownProductWeight = false;

            int longestProductNameLength = 0;
            for (Product p : productList) {
                if (p.getDescription().length() > longestProductNameLength) {
                    longestProductNameLength = p.getDescription().length();
                }
            }

            for (Product p : productList) {
                if (p.isWeightUnknown()) {
                    hasUnknownProductWeight = true;
                } else {
                    totalWeightGrams += p.getTotalWeightGrams();
                }

                outputBuilder.append(String.format("%dx\t%-" + longestProductNameLength + "s | %6s%n",
                        p.getQuantity(), p.getDescription(), formatProductWeight(p)));
            }

            outputBuilder.append("\n");
            outputBuilder.append(String.format("Total: %s%n",
                    formatTotalWeight(totalWeightGrams, hasUnknownProductWeight)));
        }

        return outputBuilder.toString();
    }

    static String formatProductWeight(Product product) {
        if (product.isWeightUnknown()) {
            return "?";
        } else {
            return String.valueOf(product.getTotalWeightGrams());
        }
    }

    static String formatTotalWeight(int totalWeightGrams, boolean hasUnknownWeight) {
        String formattedWeightValue = WEIGHT_FORMAT.format(totalWeightGrams / 1000.0);

        if (!hasUnknownWeight) {
            return String.format("%s КГ", formattedWeightValue);
        } else if (totalWeightGrams != 0) {
            return String.format("%s КГ + ?", formattedWeightValue);
        } else {
            return "?";
        }
    }
}

