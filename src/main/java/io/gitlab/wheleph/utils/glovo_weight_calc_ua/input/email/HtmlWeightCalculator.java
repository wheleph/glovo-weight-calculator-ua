package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.ProductParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Example: example/glovo_confirmation_example.html
 */
public class HtmlWeightCalculator {
    public static List<Product> extractProducts(String html) {
        Document doc = Jsoup.parse(html);
        Elements productRows = doc.select(".products__row");

        return productRows.stream()
                .map(productRow -> {
                    Elements amountElements = productRow.select(".products__amount");
                    String amountText = amountElements.get(0).text().trim();

                    Elements productElements = productRow.select(".products__product");
                    String productText = productElements.get(0).text().trim();

                    int amount = ProductParser.determineAmount(amountText);
                    int weightGrams = ProductParser.determineUnitWeightGrams(productText);

                    return new Product(amount, productText, weightGrams);
                })
                .collect(Collectors.toList());
    }
}
