package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.output.ProductTableFormatter;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsole;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsoleModule;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterException;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterParser;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterParserGuiceModule;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReader;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReaderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Launcher {

    private static final Logger logger = LogManager.getLogger(Launcher.class);

    public static void main(String[] args) {
        logger.info(">> Launching...");
        try {
            Module loggingConsoleModule = new LoggingConsoleModule();
            Injector bootstrapInjector = Guice.createInjector(loggingConsoleModule, new ProgramParameterParserGuiceModule());
            ProgramParameterParser programParameterParser = bootstrapInjector.getInstance(ProgramParameterParser.class);
            LoggingConsole console = bootstrapInjector.getInstance(LoggingConsole.class);

            try {
                ProgramParameters programParams = programParameterParser.parse(args);

                Injector mainInjector = Guice.createInjector(loggingConsoleModule, new MainGuiceModule(programParams.getInputType()));
                InputReader inputReader = mainInjector.getInstance(InputReader.class);
                ProductTableFormatter productTableFormatter = mainInjector.getInstance(ProductTableFormatter.class);

                List<Product> productList = inputReader.read(programParams);
                String output = productTableFormatter.format(productList);

                console.print(output);
            } catch (ProgramParameterException | InputReaderException e) {
                String exceptionForUser = formatExceptionForUser(e);
                console.errPrint(exceptionForUser);
                logger.error("", e);
            }
        } catch (Exception e) {
            String exceptionForUser = formatExceptionForUser(e);
            System.err.println(exceptionForUser);
            logger.error("", e);
        } finally {
            logger.info(">> Finished");
        }
    }

    private static String formatExceptionForUser(Exception e) {
        StringBuilder formattedMessage = new StringBuilder("Error occurred:\n");

        formattedMessage.append("\t").append(e.getMessage()).append("\n");

        Throwable cause = e.getCause();
        while (cause != null) {
            formattedMessage.append("\t").append(cause.getMessage()).append("\n");
            cause = cause.getCause();
        }

        formattedMessage.append("You can find more details in a log file located in 'log'").append("\n");

        return formattedMessage.toString();
    }
}
