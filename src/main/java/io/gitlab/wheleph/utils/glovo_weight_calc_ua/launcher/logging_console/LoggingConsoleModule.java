package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console;

import com.google.inject.AbstractModule;

public class LoggingConsoleModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(LoggingConsole.class)
                .to(DefaultLoggingConsole.class);
    }
}
