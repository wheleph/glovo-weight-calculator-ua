package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.props;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.InputType;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterException;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterParser;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * # glovo.input.type=file
 * #
 * # glovo.input.file.name=
 *
 * glovo.input.type=email
 *
 * glovo.input.email.host=imap.gmail.com
 * glovo.input.email.port=993
 * # glovo.input.email.user=
 * # glovo.input.email.password=
 * glovo.input.email.folder=INBOX
 * glovo.input.email.fetchSize=50
 */
public class SysPropsProgramParameterParser implements ProgramParameterParser {
    private final String usageString;
    private final Properties rawConfig;

    @Inject
    public SysPropsProgramParameterParser(@Named("cliConfig") Properties config) throws IOException {
        usageString = getResourceFileAsString("/usage.txt");
        this.rawConfig = config;
    }

    @Override
    public ProgramParameters parse(String[] args) throws ProgramParameterException {
        Properties mergedConfig = getMergedProperties();

        InputType inputType = convertInputType(mergedConfig.getProperty("glovo.input.type"));
        Path inputFile = stringToPath(mergedConfig.getProperty("glovo.input.file.name"));
        Integer emailPort = stringToInteger(mergedConfig.getProperty("glovo.input.email.port"));
        Integer emailFetchSize = stringToInteger(mergedConfig.getProperty("glovo.input.email.fetch_size"));

        ProgramParameters programParameters = new ProgramParameters.Builder()
                .setInputType(inputType)
                .setInputFile(inputFile)
                .setEmailHost(mergedConfig.getProperty("glovo.input.email.host"))
                .setEmailPort(emailPort)
                .setEmailUsername(mergedConfig.getProperty("glovo.input.email.user"))
                .setEmailUserPassword(mergedConfig.getProperty("glovo.input.email.password"))
                .setEmailFolder(mergedConfig.getProperty("glovo.input.email.folder"))
                .setEmailFetchSize(emailFetchSize)
                .build();

        validateGlobal(programParameters);

        return programParameters;
    }

    private Properties getMergedProperties() throws ProgramParameterException {
        Path configFile = stringToPath(rawConfig.getProperty("glovo.config_file"));

        Properties mergedProperties = new Properties();
        try {
            if (configFile != null) {
                Properties defaultProperties = new Properties();
                defaultProperties.load(Files.newBufferedReader(configFile, StandardCharsets.UTF_8));
                mergedProperties.putAll(defaultProperties);
            }
        } catch (IOException e) {
            throw new ProgramParameterException(String.format("Error while reading configuration from file '%s'", configFile), e);
        }

        mergedProperties.putAll(rawConfig);
        return mergedProperties;
    }

    private InputType convertInputType(String inputType) throws ProgramParameterException {
        return switch (inputType) {
            case "file" -> InputType.FILE;
            case "email" -> InputType.EMAIL;
            default -> throw new ProgramParameterException(String.format("Unknown input type: %s", inputType));
        };
    }

    private Path stringToPath(String input) {
        return input == null ?
                null :
                Paths.get(input);
    }

    private Integer stringToInteger(String input) {
        return input == null ?
                null :
                Integer.parseInt(input);
    }

    private void validateGlobal(ProgramParameters programParameters) throws ProgramParameterException {
        if (programParameters.getInputType() == InputType.FILE) {
            checkParameterNotNull(programParameters.getInputFile(), "Input file is not specified");
            if (!Files.exists(programParameters.getInputFile())) {
                throw new ProgramParameterException(prepareExceptionMessage("Input file does not exist"));
            }
        } else if(programParameters.getInputType() == InputType.EMAIL) {
            checkParameterNotNull(programParameters.getEmailHost(), "Mail host not specified");
            checkParameterNotNull(programParameters.getEmailPort(), "Mail port not specified");
            checkParameterNotNull(programParameters.getEmailFolder(), "Mail folder not specified");
            checkParameterNotNull(programParameters.getEmailFetchSize(), "Mail fetch size not specified");
        }
    }

    private void checkParameterNotNull(Object p, String errorMessage) throws ProgramParameterException {
        if (p == null) {
            throw new ProgramParameterException(prepareExceptionMessage(errorMessage));
        }
    }

    private String prepareExceptionMessage(String exceptionMessage) {
        return String.format("%s%n%s", exceptionMessage, usageString);
    }

    private String getResourceFileAsString(String fileName) throws IOException {
        try (InputStream is = getClass().getResourceAsStream(fileName)) {
            if (is == null) {
                return null;
            }
            try (InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
                 BufferedReader reader = new BufferedReader(isr)) {

                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}