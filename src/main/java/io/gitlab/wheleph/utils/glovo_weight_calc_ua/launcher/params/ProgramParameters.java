package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params;

import java.nio.file.Path;
import java.util.Objects;

public class ProgramParameters {
    private static final int DEFAULT_EMAIL_PORT = 993;
    private static final String DEFAULT_EMAIL_FOLDER = "INBOX";
    private static final int DEFAULT_EMAIL_FETCH_SIZE = 15;

    private final InputType inputType;

    private final Path inputFile;

    private final String emailHost;
    private final Integer emailPort;
    private final String emailUsername;
    private final String emailUserPassword;
    private final String emailFolder;
    private final Integer emailFetchSize;

    private ProgramParameters(InputType inputType,
                             Path inputFile,
                             String emailHost,
                             Integer emailPort,
                             String emailUsername,
                             String emailUserPassword,
                             String emailFolder,
                             Integer emailFetchSize) {
        this.inputType = inputType;
        this.inputFile = inputFile;
        this.emailHost = emailHost;
        this.emailPort = Objects.requireNonNullElse(emailPort, DEFAULT_EMAIL_PORT);
        this.emailUsername = emailUsername;
        this.emailUserPassword = emailUserPassword;
        this.emailFolder = Objects.requireNonNullElse(emailFolder, DEFAULT_EMAIL_FOLDER);
        this.emailFetchSize = Objects.requireNonNullElse(emailFetchSize, DEFAULT_EMAIL_FETCH_SIZE);
    }

    public InputType getInputType() {
        return inputType;
    }

    public Path getInputFile() {
        return inputFile;
    }

    public String getEmailHost() {
        return emailHost;
    }

    public Integer getEmailPort() {
        return emailPort;
    }

    public String getEmailUsername() {
        return emailUsername;
    }

    public String getEmailUserPassword() {
        return emailUserPassword;
    }

    public String getEmailFolder() {
        return emailFolder;
    }

    public Integer getEmailFetchSize() {
        return emailFetchSize;
    }

    public static class Builder {
        private InputType inputType;
        private Path inputFile;
        private String emailHost;
        private Integer emailPort;
        private String emailUsername;
        private String emailUserPassword;
        private String emailFolder;
        private Integer emailFetchSize;

        public Builder setInputType(InputType inputType) {
            this.inputType = inputType;
            return this;
        }

        public Builder setInputFile(Path inputFile) {
            this.inputFile = inputFile;
            return this;
        }

        public Builder setEmailHost(String emailHost) {
            this.emailHost = emailHost;
            return this;
        }

        public Builder setEmailPort(Integer emailPort) {
            this.emailPort = emailPort;
            return this;
        }

        public Builder setEmailUsername(String emailUsername) {
            this.emailUsername = emailUsername;
            return this;
        }

        public Builder setEmailUserPassword(String emailUserPassword) {
            this.emailUserPassword = emailUserPassword;
            return this;
        }

        public Builder setEmailFolder(String emailFolder) {
            this.emailFolder = emailFolder;
            return this;
        }

        public Builder setEmailFetchSize(Integer emailFetchSize) {
            this.emailFetchSize = emailFetchSize;
            return this;
        }

        public ProgramParameters build() {
            return new ProgramParameters(
                    inputType,
                    inputFile,
                    emailHost,
                    emailPort,
                    emailUsername,
                    emailUserPassword,
                    emailFolder,
                    emailFetchSize
            );
        }
    }
}
