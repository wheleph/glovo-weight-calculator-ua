package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPSSLStore;
import com.sun.mail.imap.SortTerm;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsole;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReader;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReaderException;

import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FromStringTerm;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class EmailInputReader implements InputReader {
    private static final String VALID_GLOVO_CONTENT_TYPE = "text/html";
    private static final String VALID_GLOVO_DOMAIN = "@glovoapp.com";

    private static final DateFormat RECEIVED_DATE_FORMAT = new SimpleDateFormat("MM/dd HH:mm");

    private final LoggingConsole console;

    @Inject
    public EmailInputReader(LoggingConsole console) {
        this.console = console;
    }

    @Override
    public List<Product> read(ProgramParameters programParams) throws InputReaderException {
        try {
            Message[] messages = fetchSortedMessages(programParams);

            if (messages.length == 0) {
                console.printf("No email messages from Glovo (%s) found%n", VALID_GLOVO_DOMAIN);
                return Collections.emptyList();
            } else {
                Message selectedMessage = selectMessage(messages);
                String receivedDateStr = RECEIVED_DATE_FORMAT.format(selectedMessage.getReceivedDate());
                console.printf("Processing\t%s %s\t%s%n",
                        receivedDateStr,
                        selectedMessage.getFrom()[0].toString(),
                        selectedMessage.getSubject());

                try (Folder folder = selectedMessage.getFolder()) {
                    folder.open(Folder.READ_ONLY);

                    validateSelectedMessageBasic(selectedMessage.getFrom(), selectedMessage.getContentType());

                    // It's safe to do the explicit cast here
                    // since we already validated above that content type is 'text/html'
                    String html = (String) selectedMessage.getContent();

                    return HtmlWeightCalculator.extractProducts(html);
                }
            }
        } catch (MessagingException | IOException e) {
            throw new InputReaderException("Error fetching information from emails", e);
        }
    }

    private Message[] fetchSortedMessages(ProgramParameters programParams)
            throws InputReaderException, MessagingException {
        Properties props = System.getProperties();

        Authenticator authenticator = new CustomEmailAuthenticator(
                programParams.getEmailUsername(),
                programParams.getEmailUserPassword(),
                console
        );
        Session session = Session.getInstance(props, authenticator);
        try (IMAPSSLStore store = (IMAPSSLStore) session.getStore("imaps")) {
            store.connect(programParams.getEmailHost(),
                    programParams.getEmailPort(),
                    null,
                    null);

            Folder rootMailFolder = store.getDefaultFolder();
            console.printf("Opening folder: %s%n", programParams.getEmailFolder());
            try (IMAPFolder mailFolder = (IMAPFolder) rootMailFolder.getFolder(programParams.getEmailFolder())) {
                mailFolder.open(Folder.READ_ONLY);

                Message[] messages = getGlovoMessagesSorted(store, mailFolder);
                int numberOfMessages = Math.min(messages.length, programParams.getEmailFetchSize());
                return Arrays.copyOf(messages, numberOfMessages);
            } catch (FolderNotFoundException e) {
                StringBuilder errorMessageBuilder = new StringBuilder()
                        .append(String.format("Folder %s not found%n", programParams.getEmailFolder()))
                        .append("List of available folders:\n");
                formatFolderWithSubfolders(errorMessageBuilder, rootMailFolder, 0);

                throw new InputReaderException(errorMessageBuilder.toString(), e);
            }
        }
    }

    private Message[] getGlovoMessagesSorted(IMAPSSLStore store, IMAPFolder mailFolder) throws MessagingException {
        Message[] messages;
        // Check if the IMAP server supports SORT extension (RFC 5256)
        // Notably Gmail doesn't support it
        FromStringTerm fromGlovoTerm = new FromStringTerm(VALID_GLOVO_DOMAIN);
        if (store.hasCapability("SORT*")) {
            messages = mailFolder.getSortedMessages(
                    new SortTerm[] { SortTerm.REVERSE, SortTerm.ARRIVAL },
                    fromGlovoTerm);
            fetchMessageEnvelopes(mailFolder, messages);
        } else {
            messages = mailFolder.search(fromGlovoTerm);
            fetchMessageEnvelopes(mailFolder, messages);
            Arrays.sort(messages, (o1, o2) -> {
                try {
                    if (o1.getReceivedDate().after(o2.getReceivedDate())) {
                        return -1;
                    } else if (o1.getReceivedDate().before(o2.getReceivedDate())) {
                        return 1;
                    } else {
                        return 0;
                    }
                } catch (MessagingException e) {
                    // This exception should never occur since at this point the dates
                    // are already pre-fetched
                    throw new RuntimeException("Error while sorting email messages", e);
                }
            });
        }
        return messages;
    }

    /**
     * Optimization: batch fetch of message envelopes
     */
    private void fetchMessageEnvelopes(IMAPFolder mailFolder, Message[] messages) throws MessagingException {
        FetchProfile fp = new FetchProfile();
        fp.add(FetchProfile.Item.ENVELOPE);
        mailFolder.fetch(messages, fp);
    }

    private Message selectMessage(Message[] messages) throws MessagingException {
        for (int i = 0; i < messages.length; i++) {
            Message m = messages[i];
            int dispIndex = i + 1;
            String receivedDateStr = RECEIVED_DATE_FORMAT.format(m.getReceivedDate());
            console.printf("> %2d. %s %s\t%s%n",
                    dispIndex,
                    receivedDateStr,
                    m.getFrom()[0].toString(),
                    m.getSubject());
        }

        while (true) {
            console.print("Provide the index of the email you'd like to process (default is 1): ");
            String selectedDispIndexStr = console.readLine();
            try {
                int selectedDispIndex = (selectedDispIndexStr.isEmpty())? 1 : Integer.parseInt(selectedDispIndexStr);

                if (selectedDispIndex >= 1 && selectedDispIndex <= messages.length) {
                    return messages[selectedDispIndex - 1];
                } else {
                    console.printf("The index provided (%s) isn't within range [%s, %s].%n",
                            selectedDispIndex, 1, messages.length);
                }
            } catch (NumberFormatException e) {
                console.printf("The index provided (%s) cannot be parsed.%n", selectedDispIndexStr);
            }
        }
    }

    private void formatFolderWithSubfolders(StringBuilder sb, Folder folder, int level) throws MessagingException {
        String indentation = " ".repeat(Math.max(0, level));
        sb.append(indentation).append(folder.getFullName()).append("\n");

        Folder[] subFolders = folder.list();

        for (Folder subFolder : subFolders) {
            formatFolderWithSubfolders(sb, subFolder, level + 1);
        }
    }

    static void validateSelectedMessageBasic(Address[] from, String contentType) throws InputReaderException {
        if (from.length != 1) {
            throw new InputReaderException("This doesn't appear to be an email from Glovo. " +
                    "'From' should contain exactly one address.");
        }

        if (!(from[0] instanceof InternetAddress)
                || (((InternetAddress) from[0]).getAddress() == null)) {

            String errorMessage = String.format("This doesn't appear to be an email from Glovo. " +
                            "'From' should contain '%s'.", VALID_GLOVO_DOMAIN);
            throw new InputReaderException(errorMessage);
        }

        if (!((InternetAddress) from[0]).getAddress().contains(VALID_GLOVO_DOMAIN)) {
            String errorMessage = String.format("This doesn't appear to be an email from Glovo. " +
                        "'From' should contain '%s' but was '%s'.",
                        VALID_GLOVO_DOMAIN,
                        ((InternetAddress) from[0]).getAddress());

            throw new InputReaderException(errorMessage);
        }

        if (contentType == null) {
            String errorMessage = String.format("This doesn't appear to be an email from Glovo. " +
                    "'Content-Type' should be '%s' but was missing.", VALID_GLOVO_CONTENT_TYPE);
            throw new InputReaderException(errorMessage);
        }

        if (!contentType.toLowerCase().contains(VALID_GLOVO_CONTENT_TYPE)) {
            String errorMessage = String.format("This doesn't appear to be an email from Glovo. " +
                    "'Content-Type' should be '%s' but was '%s'.", VALID_GLOVO_CONTENT_TYPE, contentType);
            throw new InputReaderException(errorMessage);
        }
    }
}
