package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.file;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsole;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReader;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReaderException;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.ProductParser;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/*
Example:

2x	СІК ДИТЯЧИЙ ПЕРСИК З М'ЯКОТТЮ 200МЛ ЧУДО-ЧАДО
18,6 грн.
2x	СЛОЙКА ЯБЛУЧНА 75Г
17 грн.
2x	ПОНЧИК З НАЧИНКОЮ 55Г
6,78 грн.
2x	БАНАН 1КГ
64 грн.
1x	ЗАКВАСКА 2.5% П/П 900Г ЯГОТИНСЬКА
31,75 грн.
2x	СИРОК ТВОРОЖНИЙ СОЛОДКИЙ З КУРАГОЮ 10% 90Г ЯГОТИНСЬКИЙ
30,14 грн.
2x	СИР К/М 5% 202Г СЛОВ'ЯНОЧКА
52 грн.
2x	ХЛІБ БІЛОРУСЬКИЙ 350Г
28,7 грн.
1x	ЗЕФІР БІЛО-РОЖЕВИЙ 350Г ЖАКО
37,85 грн.
1x	СІК АПЕЛЬСИН 950МЛ SANDORA
36,69 грн.
2x	МАНДАРИН 1КГ
61,9 грн.
1x	СОУС БУРГЕР 200Г ТОРЧИН
14,95 грн.
2x	ЯБЛУКО РЕД ЧІФ 1КГ
43,02 грн.
 */
public class FileInputReader implements InputReader {
    Pattern productLinePattern = Pattern.compile("^(\\d+x)\t(.*)$");

    private final LoggingConsole console;

    @Inject
    public FileInputReader(LoggingConsole console) {
        this.console = console;
    }

    public List<Product> read(ProgramParameters programParams) throws InputReaderException {
        Path receiptFile = programParams.getInputFile();
        console.printf("Using file %s%n%n", receiptFile);
        try {
            List<String> receiptLineList = Files.readAllLines(receiptFile, StandardCharsets.UTF_8);

            return receiptLineList.stream()
                    .map(receiptLine -> productLinePattern.matcher(receiptLine))
                    .filter(Matcher::matches)
                    .map(m -> {
                        String amountText = m.group(1);
                        int amount = ProductParser.determineAmount(amountText);

                        String productText = m.group(2);
                        int weightPerUnitGrams = ProductParser.determineUnitWeightGrams(productText);

                        return new Product(amount, productText, weightPerUnitGrams);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new InputReaderException(String.format("Error reading data from file %s", receiptFile), e);
        }
    }
}