package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params;

public enum InputType {
    FILE, EMAIL
}
