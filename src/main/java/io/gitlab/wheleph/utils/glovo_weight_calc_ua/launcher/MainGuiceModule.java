package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher;

import com.google.inject.AbstractModule;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.output.ProductTableFormatter;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.output.TabularProductTableFormatter;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.InputType;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReader;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email.EmailInputReader;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.file.FileInputReader;

public class MainGuiceModule extends AbstractModule {
    private final InputType inputType;

    public MainGuiceModule(InputType inputType) {
        this.inputType = inputType;
    }

    @Override
    protected void configure() {
        if (inputType == InputType.FILE) {
            bind(InputReader.class)
                    .to(FileInputReader.class);
        } else if (inputType == InputType.EMAIL) {
            bind(InputReader.class)
                    .to(EmailInputReader.class);
        }

        bind(ProductTableFormatter.class)
                .to(TabularProductTableFormatter.class);
    }
}
