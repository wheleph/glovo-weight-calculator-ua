package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsole;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class CustomEmailAuthenticator extends Authenticator {
    private String username;
    private String password;
    private final LoggingConsole console;

    public CustomEmailAuthenticator(String username, String password, LoggingConsole console) {
        this.username = username;
        this.password = password;
        this.console = console;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        if (username == null || password == null) {
            console.println("Please provide email authentication details.");
            if (username == null) {
                console.print("Username: ");
                username = console.readLine();
            } else {
                console.printf("Username: %s%n", username);
            }

            if (password == null) {
                password = console.readPassword("Password: ");
            } else {
                console.println("Password: (already provided)");
            }
        }

        return new PasswordAuthentication(username, password);
    }
}
