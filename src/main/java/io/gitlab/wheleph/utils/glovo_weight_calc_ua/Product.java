package io.gitlab.wheleph.utils.glovo_weight_calc_ua;

import java.util.Objects;

public class Product {
    public static final int WEIGHT_UNKNOWN = -1;

    private final int quantity;
    private final String description;
    private final int weightPerUnitGrams;

    public Product(int quantity, String description, int weightPerUnitGrams) {
        this.quantity = quantity;
        this.description = description;
        this.weightPerUnitGrams = weightPerUnitGrams;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDescription() {
        return description;
    }

    public int getWeightPerUnitGrams() {
        return weightPerUnitGrams;
    }

    public boolean isWeightUnknown() {
        return (weightPerUnitGrams == WEIGHT_UNKNOWN);
    }

    public int getTotalWeightGrams() {
        if (isWeightUnknown()) {
            return WEIGHT_UNKNOWN;
        } else {
            return quantity * weightPerUnitGrams;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return quantity == product.quantity &&
                weightPerUnitGrams == product.weightPerUnitGrams &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, description, weightPerUnitGrams);
    }
}
