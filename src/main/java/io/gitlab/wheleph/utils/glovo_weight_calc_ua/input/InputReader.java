package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters;

import java.util.List;

public interface InputReader {
    List<Product> read(ProgramParameters programParams) throws InputReaderException;
}
