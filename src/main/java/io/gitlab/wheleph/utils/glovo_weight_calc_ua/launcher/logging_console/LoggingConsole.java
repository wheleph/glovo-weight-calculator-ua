package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console;

public interface LoggingConsole {

    void print(String message);
    void println(String message);
    void printf(String format, Object... args);

    void errPrint(String message);
    void errPrintln(String message);
    void errPrintf(String format, Object... args);

    String readLine();
    String readPassword(String prompt);
}
