package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input;

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductParser {
    private static final Pattern GR_PATTERN = Pattern.compile("(?iu:\\b(\\d+)Г\\b)");
    private static final Pattern KG_PATTERN = Pattern.compile("(?iu:\\b(\\d+)КГ\\b)");
    private static final Pattern KG2_PATTERN = Pattern.compile("(?iu:\\bКГ\\b)");
    private static final Pattern ML_PATTERN = Pattern.compile("(?iu:\\b(\\d+)МЛ\\b)");
    private static final Pattern L_PATTERN = Pattern.compile("(?iu:\\b(\\d+)Л\\b)");

    /**
     * Example: "2x" -> 2
     */
    public static int determineAmount(String amountText) {
        // Cut the 'x' character at the end
        return Integer.parseInt(amountText.substring(0, amountText.length() - 1));
    }

    /**
     * Example: "ЯБЛУКО РЕД ЧІФ 1КГ" -> 1000
     */
    public static int determineUnitWeightGrams(String productText) {
        Matcher grMatcher = GR_PATTERN.matcher(productText);
        Matcher kgMatcher = KG_PATTERN.matcher(productText);
        Matcher kg2Matcher = KG2_PATTERN.matcher(productText);
        Matcher mlMatcher = ML_PATTERN.matcher(productText);
        Matcher lMatcher = L_PATTERN.matcher(productText);

        if (grMatcher.find()) {
            return getWeight(grMatcher);
        } if (kgMatcher.find()) {
            return 1000 * getWeight(kgMatcher);
        } if (kg2Matcher.find()) {
            return 1000;
        } else if (mlMatcher.find()) {
            return getWeight(mlMatcher);
        } else if (lMatcher.find()) {
            return 1000 * getWeight(lMatcher);
        } else {
            return Product.WEIGHT_UNKNOWN;
        }
    }

    private static int getWeight(Matcher m) {
        return Integer.parseInt(m.group(1));
    }
}
