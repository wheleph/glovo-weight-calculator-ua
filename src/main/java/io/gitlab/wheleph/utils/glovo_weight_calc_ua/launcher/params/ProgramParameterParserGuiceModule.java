package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.props.SysPropsProgramParameterParser;

import java.util.Properties;

public class ProgramParameterParserGuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ProgramParameterParser.class)
                .to(SysPropsProgramParameterParser.class);

        bind(Properties.class)
                .annotatedWith(Names.named("cliConfig"))
                .toInstance(System.getProperties());
    }
}
