package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product
import spock.lang.Specification

class HtmlWeightCalculatorTest extends Specification {
    def "sunshine scenario"() {
        given:
        String html = this.getClass().getResource("/HtmlWeightCalculatorTest/glovo_confirmation_example.html").getText()

        when:
        def products = HtmlWeightCalculator.extractProducts(html)

        then:
        products != null
        products.size() == 8
        products.get(0) == new Product(2, "СІК ДИТЯЧИЙ ПЕРСИК З М'ЯКОТТЮ 200МЛ ЧУДО-ЧАДО", 200)
        products.get(1) == new Product(2, "МЛИНЦІ З КУРКОЮ 0.370", Product.WEIGHT_UNKNOWN)
        products.get(2) == new Product(3, "ЗЕФІР БІЛО-РОЖЕВИЙ 350Г ЖАКО", 350)
        products.get(3) == new Product(2, "МОЛОКО КОРОВЯЧЕ ПАСТЕР. 2.6% Т/П 2Л ЯГОТИНСЬКЕ", 2000)
        products.get(5) == new Product(4, "ФАРШ АСОРТІ ДОМАШНІЙ ОХОЛ. В/У 1КГ", 1000)
    }
}
