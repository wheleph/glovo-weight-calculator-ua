package io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.props

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.TestUtil
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.InputType
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameterException
import spock.lang.Specification

import java.nio.file.Path

class SysPropsProgramParameterParserTest extends Specification {
    def "file input mode"() {
        given:
        Path dataFilePath = createTempDataFile()
        Properties props = new Properties()
        props.putAll(["glovo.input.type"     : "file",
                      "glovo.input.file.name": dataFilePath.toString()])
        SysPropsProgramParameterParser parameterParser = new SysPropsProgramParameterParser(props)

        when:
        def programParameters = parameterParser.parse(null)

        then:
        programParameters != null
        programParameters.inputType == InputType.FILE
        programParameters.inputFile == dataFilePath
    }

    def "reading from a file + overrides"() {
        given:
        Path dataFilePath1 = createTempDataFile()
        Path dataFilePath2 = createTempDataFile()

        Path configFile = TestUtil.createTempFile("""
                |glovo.input.type=file
                |glovo.input.file.name=${dataFilePath1.toString()}""".stripMargin())

        Properties props = new Properties()
        props.putAll(["glovo.config_file"    : configFile.toString(),
                      "glovo.input.file.name": dataFilePath2.toString()])
        SysPropsProgramParameterParser parameterParser = new SysPropsProgramParameterParser(props)

        when:
        def programParameters = parameterParser.parse(null)

        then:
        programParameters != null
        programParameters.inputType == InputType.FILE
        programParameters.inputFile == dataFilePath2

    }

    def "non-existing config file"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.config_file": "non_existent"])
        SysPropsProgramParameterParser parameterParser = new SysPropsProgramParameterParser(props)

        when:
        parameterParser.parse(null)

        then:
        thrown(ProgramParameterException.class)
    }

    def "wrong input type"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.input.type": "blah"])
        SysPropsProgramParameterParser parser = new SysPropsProgramParameterParser(props)

        when:
        parser.parse(null)

        then:
        thrown(ProgramParameterException.class)
    }

    def "missing input file"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.input.type": "file"])
        SysPropsProgramParameterParser parser = new SysPropsProgramParameterParser(props)

        when:
        parser.parse(null)

        then:
        thrown(ProgramParameterException.class)
    }

    def "non-existing input file"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.input.type"     : "file",
                      "glovo.input.file.name": "non_existent.txt"])
        SysPropsProgramParameterParser parser = new SysPropsProgramParameterParser(props)

        when:
        parser.parse(null)

        then:
        thrown(ProgramParameterException.class)
    }

    def "email input mode"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.input.type": "email",
                      "glovo.input.email.host": "imap.gmail.com",
                      "glovo.input.email.port": "995",
                      "glovo.input.email.user": "john",
                      "glovo.input.email.password": "s3cret",
                      "glovo.input.email.folder": "INBOX",
                      "glovo.input.email.fetch_size": "51"])
        SysPropsProgramParameterParser parameterParser = new SysPropsProgramParameterParser(props)

        when:
        def programParameters = parameterParser.parse(null)

        then:
        programParameters != null
        programParameters.inputType == InputType.EMAIL
        programParameters.getEmailHost() == "imap.gmail.com"
        programParameters.getEmailPort() == 995
        programParameters.getEmailUsername() == "john"
        programParameters.getEmailUserPassword() == "s3cret"
        programParameters.getEmailFolder() == "INBOX"
        programParameters.getEmailFetchSize() == 51
    }

    def "email default values"() {
        given:
        Properties props = new Properties()
        props.putAll(["glovo.input.type": "email",
                      "glovo.input.email.host": "imap.gmail.com",
                      "glovo.input.email.user": "john",
                      "glovo.input.email.password": "s3cret"])
        SysPropsProgramParameterParser parameterParser = new SysPropsProgramParameterParser(props)

        when:
        def programParameters = parameterParser.parse(null)

        then:
        programParameters != null
        programParameters.inputType == InputType.EMAIL
        programParameters.getEmailPort() != null
        programParameters.getEmailFolder() != null
        programParameters.getEmailFetchSize() != null
    }

    private static Path createTempDataFile() {
        TestUtil.createTempFile("""
                |2x\tСІК ДИТЯЧИЙ ПЕРСИК З М'ЯКОТТЮ 200МЛ ЧУДО-ЧАДО
                |18,6 грн.
                |2x\tСЛОЙКА ЯБЛУЧНА 75Г
                |17 грн.""".stripMargin())
    }
}
