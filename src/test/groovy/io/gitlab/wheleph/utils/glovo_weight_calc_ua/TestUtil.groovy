package io.gitlab.wheleph.utils.glovo_weight_calc_ua

import java.nio.file.Files
import java.nio.file.Path

class TestUtil {
    static Path createTempFile(String content) {
        Path path = Files.createTempFile("", "")
        Files.writeString(path, content)
        path.toFile().deleteOnExit()
        return path
    }
}
