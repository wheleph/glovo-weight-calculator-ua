package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product
import spock.lang.Specification

class ProductParserTest extends Specification {
    def "determine weight"() {
        expect:
        ProductParser.determineUnitWeightGrams(input) == value

        where:
        input | value
        "МАНДАРИН 1КГ" | 1000
        "МАНДАРИН 20КГ" | 20000
        "мандарин 20кг" | 20000
        "ХЛІБ БІЛОРУСЬКИЙ 350Г" | 350
        "СІК АПЕЛЬСИН 950МЛ SANDORA" | 950
        "БУБЛИК 1ШТ" | Product.WEIGHT_UNKNOWN
        "МОЛОКО КОРОВЯЧЕ ПАСТЕР. 2.6% Т/П 2Л ЯГОТИНСЬКЕ" | 2000
        "ЯБЛУКО КГ" | 1000
        "СОК 200МЛ БЕЗ КГМО" | 200
    }

    def "determine amount"() {
        expect:
        ProductParser.determineAmount("2x") == 2
    }
}
