package io.gitlab.wheleph.utils.glovo_weight_calc_ua

import spock.lang.Specification

class ProductTest extends Specification {
    def "get total weight"() {
        expect:
        new Product(quantiny, desc, weightPerUnit).getTotalWeightGrams() == result

        where:
        quantiny    |   desc    |   weightPerUnit           |   result
        2           |   "abc"   |   1000                    |   2000
        2           |   "def"   |   Product.WEIGHT_UNKNOWN  |   Product.WEIGHT_UNKNOWN
    }
}
