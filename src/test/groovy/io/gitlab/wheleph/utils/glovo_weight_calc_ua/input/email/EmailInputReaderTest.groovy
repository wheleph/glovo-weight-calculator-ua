package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.email

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.InputReaderException
import spock.lang.Specification

import javax.mail.Address
import javax.mail.internet.InternetAddress

class EmailInputReaderTest extends Specification {
    def "basic validation sunshine scenario"() {
        when:
        EmailInputReader.validateSelectedMessageBasic(
                [new InternetAddress("no-reply@glovoapp.com")] as Address[],
                "TEXT/HTML; charset=utf-8"
        )

        then:
        noExceptionThrown()
    }

    def "basic validation many 'From' addresses"() {
        when:
        EmailInputReader.validateSelectedMessageBasic(
                [new InternetAddress("no-reply@glovoapp.com"), new InternetAddress("no-reply@domain.com")] as Address[],
                "text/html"
        )

        then:
        thrown(InputReaderException.class)
    }

    def "basic validation wrong domain"() {
        when:
        EmailInputReader.validateSelectedMessageBasic(
                [new InternetAddress("no-reply@domain.com")] as Address[],
                "text/html"
        )

        then:
        thrown(InputReaderException.class)
    }

    def "basic validation wrong content type"() {
        when:
        EmailInputReader.validateSelectedMessageBasic(
                [new InternetAddress("no-reply@glovoapp.com")] as Address[],
                "text/plain"
        )

        then:
        thrown(InputReaderException.class)
    }

    def "basic validation missing content type"() {
        when:
        EmailInputReader.validateSelectedMessageBasic(
                [new InternetAddress("no-reply@glovoapp.com")] as Address[],
                null
        )

        then:
        thrown(InputReaderException.class)
    }
}
