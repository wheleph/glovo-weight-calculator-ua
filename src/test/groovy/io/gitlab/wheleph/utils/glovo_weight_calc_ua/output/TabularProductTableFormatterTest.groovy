package io.gitlab.wheleph.utils.glovo_weight_calc_ua.output

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product
import spock.lang.Specification

class TabularProductTableFormatterTest extends Specification {

    def "sunshine scenario"() {
        given:
        List<Product> products = [new Product(2, "milk", 1500),
                                  new Product(3, "donut", Product.WEIGHT_UNKNOWN)]
        TabularProductTableFormatter formatter = new TabularProductTableFormatter()

        when:
        def output = formatter.format(products)

        then:
        output == """|2x\tmilk  |   3000
            |3x\tdonut |      ?
            |
            |Total: 3 КГ + ?
            |""".stripMargin()
    }

    def "empty product list"() {
        given:
        TabularProductTableFormatter formatter = new TabularProductTableFormatter()

        when:
        String output = formatter.format([])

        then:
        output == "No products available\n"
    }

    def "format total weight"() {
        expect:
        TabularProductTableFormatter.formatTotalWeight(totalWeight, hasUnknownWeight) == output

        where:
        totalWeight |   hasUnknownWeight    | output
        1000        |   false               | "1 КГ"
        1000        |   true                | "1 КГ + ?"
        0           |   true                | "?"
        0           |   false               | "0 КГ"
    }

    def "format product weight"() {
        expect:
        TabularProductTableFormatter.formatProductWeight(product) == output

        where:
        product                                        |   output
        new Product(2, "abc", 1000) |   "2000"
        new Product(2, "def", Product.WEIGHT_UNKNOWN)  |   "?"
    }
}


