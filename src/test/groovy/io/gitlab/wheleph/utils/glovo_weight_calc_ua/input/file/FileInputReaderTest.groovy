package io.gitlab.wheleph.utils.glovo_weight_calc_ua.input.file

import io.gitlab.wheleph.utils.glovo_weight_calc_ua.Product
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.TestUtil
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.logging_console.LoggingConsole
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.InputType
import io.gitlab.wheleph.utils.glovo_weight_calc_ua.launcher.params.ProgramParameters
import spock.lang.Specification

import java.nio.file.Path

class FileInputReaderTest extends Specification {
    def "sunshine scenario"() {
        given:
        FileInputReader fileInputReader = new FileInputReader(Mock(LoggingConsole.class))
        Path inputFilePath = TestUtil.createTempFile("""
                |2x\tСІК ДИТЯЧИЙ ПЕРСИК З М'ЯКОТТЮ 200МЛ ЧУДО-ЧАДО
                |18,6 грн.
                |2x\tСЛОЙКА ЯБЛУЧНА 75Г
                |17 грн.
                """.stripMargin())

        ProgramParameters params = new ProgramParameters.Builder()
            .setInputType(InputType.FILE)
            .setInputFile(inputFilePath)
            .build()

        when:
        List<Product> products = fileInputReader.read(params)

        then:
        products.size() == 2
    }
}
