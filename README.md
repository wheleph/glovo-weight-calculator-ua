### Overview

According to [Glovo FAQ](https://glovoapp.com/en/faq) the total weight of an order 
should be under 9 kg:

> Our couriers use motorcycles or bikes so they can carry whatever you want, 
> as long as it’s under 9 kg in weight (3-4 shopping bags in volume)

If you shop around at Ukrainian supermarkets via Glovo you can find that 
vast majority of items do have net weight specified in their descriptions. 
But their mobile app as well as their website still do not show the total weight
of an order.

To be sure that you stay under 9 kg limit you can either use pen and paper or
this command-line application!

This application consumes Glovo confirmations and receipts either from a text file
or directly from your email inbox via IMAPS.

This is just-for-fun project created during COVID-19 lockdown so if you think
that it's stupid to find the total weight of already confirmed orders,
you're absolutely correct. But I also created 
[Google Chrome extension](https://github.com/wheleph/glovo-weight-calculator-ua-chrome-ext) 
that allows you to see the total weight while you're shopping at glovoapp.com!

### Usage

To build the application you need to have Java 14 installed. Run this command in the 
root directly of this repository:

```shell script
./mvn package
```

This will create a fat jar (binary that includes the application and all its dependencies).

The easiest way to run it is via `run_config.sh`. For that you also need to supply
a configuration file. Example of the file:

```shell script
~$ cat $HOME/.glovo/config.properties
glovo.input.type=email

glovo.input.email.host=imap.gmail.com
glovo.input.email.port=993
glovo.input.email.user=<redacted>
glovo.input.email.password=<redacted>
glovo.input.email.folder=[Gmail]/All Mail
glovo.input.email.fetch_size=50
```  

#### Gmail access

If you have 2-factor Google authentication set up then in order to access your email inbox via IMAP you have to use
so-called [App passwords](https://support.google.com/accounts/answer/185833?hl=en#)

### Limitations

The code expects to find weight specification in Ukrainian/Russian units
(like КГ, Г, Л, МЛ). So it should be modified to make it work for 
other countries or languages.
